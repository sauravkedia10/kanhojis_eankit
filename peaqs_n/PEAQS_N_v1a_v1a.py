#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 16 11:47:29 2021

@author: saurav
"""

import datetime
import numpy as np
import pandas as pd
import talib
import time
import os
from collections import defaultdict
from kanhoji import config
from candlesarray import CandlesArray, KanhojiIndexedSet
from kanhoji.price_parser import PriceParser
#from kanhoji.price_handler.historic_data_reader_database import HistoricalDataReader
from narada.nse_data_downloader import NSEDataDownloader
from narada.holidays import ExpiryCalendar, NSEHolidays
from narada.datafeedtickers import Tickers
#from narada.nse_corporate_actions import NSECorpActions
from kanhojimain.strategy.base_rabbitmq import TickerProxy
from kanhojimain.strategy.base_rabbitmq_positionalorder import AbstractStrategyPositional
import kanhoji.event as event
from kanhoji.price_handler.zerodha_historical_price_downloader import KiteHistoricalData
import threading
from kanhoji.initialize_models import session_scope
from kanhoji.statistics.tearsheet_live import TearsheetStatisticsLive
from kanhoji.statistics.tearsheet_portfolio_live import TearsheetPortfolioLive
import kanhoji.globals as g
import redis
from sqlalchemy.sql import func
#These are special types of dict
from boltons.dictutils import OrderedMultiDict, OneToOne
#from kanhoji.statistics.tearsheet_live import TearsheetStatisticsLive
#from abc import ABCMeta, abstractmethod, ABC
from pathos.pools import ThreadPool 
#from kanhoji.rabbitmq_consumer import ReconnectingRabbitMqConsumer
#from kanhoji.rabbitmq_publisher import RabbitmqPublisher
#from kanhoji.price_handler.zerodhaWebsocketPriceHandler2 import ZerodhaWebsocketTickPriceHandlerBasic

kanhoji_korbat_path = os.path.expanduser(
                        config['csv']['kanhoji_korbat_path']
                        )

import logging
logger = logging.getLogger(__name__)
#    to supress warnings like these:
#    __main__:320: RuntimeWarning: divide by zero encountered in true_divide
#    __main__:320: RuntimeWarning: invalid value encountered in true_divide
#    __main__:337: RuntimeWarning: invalid value encountered in greater_equal
#    __main__:337: RuntimeWarning: invalid value encountered in less_equal
#    __main__:337: RuntimeWarning: invalid value encountered in less
#    __main__:347: RuntimeWarning: invalid value encountered in greater
#        
np.seterr(divide='ignore', invalid='ignore')
np.warnings.filterwarnings('ignore')


redis_client = redis.StrictRedis(
                                 host=config['REDIS/2']['host'],
                                 port=config['REDIS/2']['port'], 
                                 db=config['REDIS/2']['db']
                                 )
        

#Strategy level params
#---------------------

TIME_ENTRYSTART = 569 # change it back to 569  
TIME_LASTSIGNAL = 870
EXIT_TIME = 15 * 60 + 22
TIME_IN_FORCE = 870
PORTFOLIO_EQUITY	= 200000
	
TIME_ENTRYSTART = ( datetime.datetime(100,1,1) +  
                    datetime.timedelta(minutes=TIME_ENTRYSTART)).time()

TIME_LASTSIGNAL = ( datetime.datetime(100,1,1) +  
                    datetime.timedelta(minutes= TIME_LASTSIGNAL)).time()


# keep in mind that TIME_IN_FORCE  and EXIT_TIME below are being overridden
# by values in the strategy subscription related table in database!

EXIT_TIME = ( datetime.datetime(100,1,1) +  
                    datetime.timedelta(minutes= EXIT_TIME)).time()

# EXIT_TIME_FORCED doesn't matter in Positinal System
EXIT_TIME_FORCED = datetime.time(23,24)

# keep it at 15:31 for this strategy and a bit higher for others.
# Because it will download and save Nifty ticker for benchmark performance.
TIME_EOD = datetime.time(15,32)

MAX_OPEN_POSITIONS = 10
#PORTFOLIOEQUITY	= 1000000

DAYSTARTTIME = datetime.time(9,15)
TIME_CANDLE_START = DAYSTARTTIME
TIME_CANDLE_END = datetime.time(15,29)
TIME_STOP_CALCS = datetime.time(15,29)


STRATEGY_START_DATE = datetime.datetime(2021, 5, 16)
STRATEGY_NO_OF_DAYS = NSEHolidays().calendar.busdaycount(STRATEGY_START_DATE, 
                                 datetime.datetime.today()
                                 )


class PEAQS_N(AbstractStrategyPositional):
    """
    PEAQS_N implements POSITIONAL Quick Singles Strategy (NIFTY version)
    from external partner ANKIT GARG (DB CAPITAL). PEAQS_B implements the
    BANKNIFTY version.
    
    The Signals for this strategy will come from webhook on merucapitals site
    and will be transmitted to this strategy via `eventk.StrategySignalEvent`.
    
    This strategy is based on kanhojis_saurav.ptest1.ptest1.PTest class
    and implements  `AbstractStrategyPositional` strategy. It is a positional 
    strategy, one which can run positions across  multiple days i.e. 
    overnight positions.
    
    At morning at initialization time, before the market opens, the 
    `orderbook_handler` will notify the strategy of open positions with a 
    `kanhoji.event.OpenPositionsInfoEvent`. Upon recieving this event, the 
    strategy will call the `self.on_open_positions_info_event` and inside it, 
    all overnight positions (including those where Entry order is placed but 
    no entery Fill has been received yet) will be retrieved and all important 
    dicts and variables like `self.tickers`, self.clientdict, 
    self.tickers_invested etc will be seeded in strategy. This method can be    
    overriden in a strategy to handle more complicated cases where base class
    implementation is not adequate.
    
    So, when stategy starts at 9.15 am (market open), all variables are already
    initizalized and the dealer can work as per usual workflow.
    
    The strategy will release `kanhoji.event.PositionalSignalEvent` which will
    create a `kanhoji.internal_order.positional.PositionalOrder` orders. This
    `PositionalOrder` is the base order for positional order. Dealer is requested
    to go through the documentation of both `PositionalSignalEvent` and
    'PositionalOrder' to get full ideal of the features provided by this order.

    While initialing the strategy in morning it will also handle two corner cases:
        a. if a client, in which there is open position, is not there in 
            self.strategy_clients, ie has no subscription to this strategy today. 
            For such cases, this client will be created and initialized in the
            strategy and added to self.strategy_clients. However, the `is_alive`
            flag for this client in `self.clientdict` will be set as False and
            there will be no fresh positions in this client. Only exisiting
            position will be managed.
            
        b. A position is open but its ticker_base is no longer in `self.tickers_base`
            say because the underlying components of index have changed. In such
            case the ticker_base (and its ticker_cash and its ticker_proxy) will 
            added to the watchlist, and historical candles will be read again
            to include this new ticker_base. Please note, a fresh entry can 
            happen in this ticker as now it is a part of the watchlist.
            
    EXECUTION PLAN for PEAQS
    -------------------------
    1. Partner Ankit will send json to mountmeru website in following format:
            data =  {
                'strategy': 'qs', 
                'instrument': 'nifty', 
                'position': '-1', 
                'spotprice': '17348.05', 
                'timestamp': '2021-09-09T03:48:00Z'
                }                        
    2. It will be recieced over webhook at endpoint \webhook_dbcapital\
        and handled in `django-kanhoji.kanhojiui.views.webhook_dbcapital` method.
    
    3. After few validation checks, this will create a dict `trigger` which contains 
        params from `eventk.StrategySignalEvent`. 
        
    4. If positon in recived data is 0, the `strategy_signal_type` will be `EXIT_MARKET`
        else it will be `NEW_ENTRY`.
    
    5. The `eventk.StrategySignalEvent` will have few deviations:
            a. ticker_base contains underlying/ticker_cash not ticker_base
            b. quantity contains no_of_lots as opposed to no_of_shares.
            c. price is just a reference price for finding ATM strike, rather than
            a price param for order.

        All of these are important field for processing the order and will be
        handled in `on_strategy_signal_event` method in this class.
    
    6. This trigger will be sent to kanhojimain over redis. `kahnoji.django_events` 
        will receive it in trading session, convert to `eventk.StrategySignalEvent`
        and send it to this class over rabbitmq.
        
    7. In the `on_strategy_signal_event` method in this class, this 
        `eventk.StrategySignalEvent` will be processed to either exit all open 
        positions or create new entries depening upon the `strategy_signal_type` 
        of this event.
    """
    def __init__(self, equity=PORTFOLIO_EQUITY, 
                 strategy_code=np.random.randint(100,999),
                 strategy_id=None, 
                 strategy_mode='live', currency='INR',
                 time_in_force=EXIT_TIME, 
                 exit_time=EXIT_TIME, 
                 dealer_id='SAURAV',
                 strategy_clients=None, 
                 watchlist='NIFTY',
                 is_tickers_futures_only=False,
                 tickers_base_type='OPTIONS',
                 is_tickers_proxy_cash=False,
                 is_tickers_proxy_futures=False,
                 is_tickers_proxy_option=True,                 
                 tickers_discard = set({}),
                 is_tickers_discard_on_board_meeting=False,
                 is_tickers_discard_banned_securities=False, 
                 is_signal_in_cash_if_future_missing=False,
                 is_signal_in_cash_if_future_in_ban=False,
                 price_handler=None,
                 rpublisher=None,
                 rconsumer=None,
                 historical_data_reader=None,
                 daystarttime=DAYSTARTTIME,
                 time_stop_calcs = TIME_STOP_CALCS,
                 time_eod= TIME_EOD,
                 time_candle_start=TIME_CANDLE_START,
                 time_candle_end=TIME_CANDLE_END,                 
                 run_calculation_every = 60,
                 generate_signals_at_fixed_time=False,
                 generate_signals_every=60,
                 candle_timeframe = 60,
                 candle_base=datetime.time(9,15),
                 is_pytest=False,
                 exit_time_forced=EXIT_TIME_FORCED,
                 benchmark='NIFTY',
                 is_stop_on_benchmark_gap=False,
                 benchmark_upper_gap=0.5,
                 benchmark_lower_gap=-0.5,
                 run_next_futures_before_expiry_days=-1,                                                   
                 run_cash_before_expiry_days=-1,
                 leverage_factor=1,
                 signal_type=g.OrderType_PositionalOrder,
                 max_open_positions=MAX_OPEN_POSITIONS,
                 is_cash_fut_basis_fixed=False,
                 is_cash_fut_basis_fixed_set_once=False,                 
                 is_allowed_mutliple_positions_same_ticker=True,
                 mtm_positive_threshold=0.9,
                 mtm_negative_threshold=0.9,
                 is_run_friday=True,
                 is_exit_automatically_on_mtm_hit=False,
                 mtm_exit_monitoring='strategy', # one of {'strategy', 'maxequity'}                                  
                 signal_event_type=event.PositionalSignalEvent,
                 time_entrystart=g.NFO_OPEN_TIME,
                 date_entrylast=datetime.datetime.today().date(),
                 date_exit=None,                 
                 ):  

        """
        Note, the params above are default ones! Doesn't mean they will be 
        passed. For example, for time_in_force and exit_time, they
        will be overridden by values specified in database.
        
        Further, not the last few parameter passed as None. please always pass
        them even if they are redundant as custom mocking objects could be passed
        in the test cases for these. Thats why they have been introduced, to give
        more flexibility while writing tests!

        """
        
        # fetch all banknifty option tickers in tickers_options
        # self.tickers_options = Tickers.fetch_tickernames_from_bhavcopy(
        #                     symbol_list='BANKNIFTY', offset=1, series='weekly')
        
        # set tickername column as index
        # self.tickers_options.set_index('tickername', inplace=True)
        
        super().__init__(equity=equity, strategy_code=strategy_code, 
                 strategy_id=strategy_id, 
                 strategy_mode=strategy_mode, currency=currency,
                 time_in_force=time_in_force, 
                 exit_time=exit_time, 
                 dealer_id=dealer_id,
                 strategy_clients=strategy_clients,
                 watchlist=watchlist,
                 is_tickers_futures_only=is_tickers_futures_only,
                 tickers_base_type=tickers_base_type,
                 is_tickers_proxy_cash=is_tickers_proxy_cash,
                 is_tickers_proxy_futures=is_tickers_proxy_futures,
                 is_tickers_proxy_option=is_tickers_proxy_option,                 
                 tickers_discard=tickers_discard,
                 is_tickers_discard_on_board_meeting=is_tickers_discard_on_board_meeting,
                 is_tickers_discard_banned_securities=is_tickers_discard_banned_securities,
                 is_signal_in_cash_if_future_missing=is_signal_in_cash_if_future_missing,
                 is_signal_in_cash_if_future_in_ban=is_signal_in_cash_if_future_in_ban,                 
                 price_handler=price_handler,
                 rpublisher=rpublisher,
                 rconsumer=rconsumer,
                 historical_data_reader=historical_data_reader,
                 daystarttime=daystarttime,
                 time_stop_calcs=time_stop_calcs,
                 time_eod=time_eod,
                 time_candle_start=time_candle_start,
                 time_candle_end=time_candle_end,                 
                 run_calculation_every=run_calculation_every,
                 generate_signals_at_fixed_time=generate_signals_at_fixed_time,
                 generate_signals_every=generate_signals_every,
                 candle_timeframe=candle_timeframe,
                 candle_base=candle_base,
                 is_pytest=is_pytest,
                 exit_time_forced=exit_time_forced,
                 benchmark='NIFTY',
                 is_stop_on_benchmark_gap=is_stop_on_benchmark_gap,
                 benchmark_upper_gap=benchmark_upper_gap,
                 benchmark_lower_gap=benchmark_lower_gap,
                 run_next_futures_before_expiry_days=run_next_futures_before_expiry_days,                                  
                 run_cash_before_expiry_days=run_cash_before_expiry_days,
                 leverage_factor=leverage_factor,
                 signal_type=signal_type,
                 max_open_positions=max_open_positions,
                 is_cash_fut_basis_fixed=is_cash_fut_basis_fixed,
                 is_cash_fut_basis_fixed_set_once=is_cash_fut_basis_fixed_set_once,
                 is_allowed_mutliple_positions_same_ticker=is_allowed_mutliple_positions_same_ticker,
                 mtm_positive_threshold=mtm_positive_threshold,
                 mtm_negative_threshold=mtm_negative_threshold,
                 is_run_friday=is_run_friday,
                 is_exit_automatically_on_mtm_hit=is_exit_automatically_on_mtm_hit,
                 mtm_exit_monitoring=mtm_exit_monitoring, # one of {'strategy', 'maxequity'}                                   
                 signal_event_type=signal_event_type,
                 time_entrystart=time_entrystart,
                 date_entrylast=date_entrylast,
                 date_exit=date_exit,                 
                 )   
        
        # self.date_entrylast set at today at init as entry can only happen today 
        # cumplosry exit next trading day. Set here and not in init as attr
        # self.nseholidays required, which is set when superclass is init.
        self.date_exit = self.nseholidays.next_trading_day(
            date=datetime.datetime.today(), offset=1)
        
        
    def strategy_place_long_entry(self, ticker_base='LT', positions=None):
        """
        Place a sample long entry order in provided ticker_base.

        Parameters
        ----------
        ticker_base : str, optional
            The ticker_base. 
            The default is 'LT'.
        
        positions : None, optional
            Just pass as None. The default is None.

        Returns
        -------
        None.

        """
        LTP_base = LTP =  self.price_handler.get_mid_price(ticker_base)
        lotsize  = self.price_handler.tickers[ticker_base].get('lotsize', 1)
        
        trigger_price = PriceParser.myround_intprice(int(LTP * 1.001))
        price = PriceParser.myround_intprice(int(LTP * 1.001))

        stoploss_trigger_price = PriceParser.myround_intprice(int(LTP*.97))
        stoploss_price = PriceParser.myround_intprice(int(LTP*.97))
        takeprofit_price = PriceParser.myround_intprice(int(LTP*1.10))
        no_of_shares = 1
        remarks = "Test"
        
        self.ticker_place_entry_order(
            ticker_base=ticker_base,
            actual_action='BUY',
            no_of_shares=no_of_shares,
            LTP_base=LTP_base,
            price=price,
            trigger_price=trigger_price,
            stoploss_price=stoploss_price,
            stoploss_trigger_price=stoploss_trigger_price,
            takeprofit_price=takeprofit_price,
            remarks=remarks,
            positions=positions
            )                  
        
        self.tickers_invested.add(ticker_base)
        self.tickers_already_invested.add(ticker_base)
        self.tickers[ticker_base]['invested'] = 1
        
        
    def generate_signals(self, time1=datetime.datetime.now()):
        """
        dummy generate_signals for demo purposes.

        Parameters
        ----------
        time1 : datetime.datetime.now(), optional
            The default is datetime.datetime.now().

        Returns
        -------
        None.

        """
        pass
        
            
    def calculate_signals(self, event):
        """right now this strategy doesnt react to price events, hence passing.""" 
        return

    def read_yesterday_dbase_data(self):
        """
        Read yesterday's data for database and return CandleArray.        
        """
        pass
            
    def get_candles(self):
        """
        Read yesterday data if not read already in CandleArray form.
        Then add todays candles in CandleArray form.
        return the combined CandleArray.
        """
       
        if self.mode=='live':
            
            # read todays candles, add the expandLast columns from yesterday to it
            # This strategy needs  today's candles and daily expandLast
            # values from yesterday as well as 10 Min and 60 Min statistics.
            
            # we will add all daily, 10min and 60 min statistics for today in 
            # this step rather than later
              
            # get todays' 1min candles.          
            self.candles = self.price_handler.candles.get(
                    tickers=sorted(self.tickers_base),
                    fields=['open', 'high', 'low', 'close', 'volume']
                    )
            
        return self.candles
    
    def calculate_signals_using_price_handler(self):
        """ 
        Run actual calculations and generate signals.
        In this algo, at 9.24 when the most liquid stocks are determined,
        we will prune the watchlist itself to those 10 tickers in
        _calculate_signals_in_continuous_loop method.
        
        """
        
        try:
        # Get the combined Candlearray for yesterday and today.
            self.calcs = self.get_candles()        
            return self.calcs
        
        except Exception as e:
            logger.exception (e)

    def client_is_allowed_to_place_order(self, client, tpo, actual_action, 
                                                                 **kwargs):
        """
        Whether a fresh Signal will be generated for a `TickerProxy` object (tpo) 
        in a particular `client` represented by its client_ntuple in 
        `self.strategy_clients`!
        
        It will look at inputs as well inspect self.clientdict[client][tpo.base] 
        like `invested`, `tickers_already_exited` and so on!        

        Parameters
        ----------
        client : client_ntuple object
            client represented by its client_ntuple object.
            see: `self.strategies_clients` attribute.
            
        tpo : TickerProxy object
            `TickerProxy` object which contain the `ticker_proxy` details.
            
        actual_action : {'BUY', 'SELL'}
            actual action for this `TickerProxy` object. 
            
            actual_action is distinct from action. 
            Example: An actual_action of 'BUY' in ACC may result in final action 
            of 'SELL' at Signal in case tpo is PE option.
            
        **kwargs : dict
            if additional params may help in making this decision, then they may
            be passsed as kwargs.
            
            In this case, kwargs will come from the method 
            `self.ticker_place_entry_order` to generate signal inside the 
            `self.generate_signals` call.

        Returns
        -------
        Bool
            If True, the client is allowed to place a fresh entry Signal in this
            `TickerProxy` object with provied actual_action and other params
            provied as kwargs.
            
        Note
        ----
        This method is what ensures whether a fresh Signal can be taken or not.
        So, it allows us to resolve situations like if a Signal has to be generated
        for a client while not for others (may be it already has a Signal in this
        tpo running already).
        """
        if self.clientdict[client][tpo.base].get('invested', 0) == 0:
            return True
        elif self.is_allowed_mutliple_positions_same_ticker:
            return True
        else:
            return False  
        
    def fill_place_exit_orders(self, fill_event3, **kwargs):
        """
        The method places Exit orders (exit_SL for stoploss and
        exit_TP for takeprofit) when a `FillEvent3` is recieved on entry side.
        

        Parameters
        ----------
        fill_event3 : kanhoji.event.Event.FillEvent3 object
            FillEvent3 sent by `trading_session` to this strategy notifying of
            a Fill in an order sent by this strategy.
            
        **kwargs : dict
            Additional parameters sent by `self.on_fill` method for strategy
            specific customization.

        Returns
        -------
        None.

        """
        pass

    def get_atm_ticker_from_strategy_signal_event(self, strategy_signal_event):
        """
        Get desired ATM option ticker to be traded.

        Parameters
        ----------
        strategy_signal_event : kanhoji.event.StrategySignalEvent
            StrategySignalEvent from django.

        Returns
        -------
        ticker_option_atm : str, 
            Ticker_atm being traded.
            
        Logic
        -----
        If position==1 (i.e. sse.actual_action==1:
    		If (days to expiry>=2)
    			use Nifty ATM CE of current expiry
    		Else
    			use Nifty ATM CE of next expiry 
	
        If position==-1 (i.e. sse.actual_action == SELL)
    		If (days to expiry>=2)
    			use Nifty ATM PE of current expiry
            Else
    			use Nifty ATM PE of next expiry 
            
        """
        sse = strategy_signal_event

        # underlying
        underlying = sse.ticker_base
        
        # Strike
        rounding_factor = 50 if self.watchlist == 'NIFTY' else 100
        strike = PriceParser.myround(sse.price, base=rounding_factor, prec=0)
        
        # option_type from action_type
        option_type = 'CE' if sse.actual_action == 'BUY' else 'PE'
        
        # Expiry
        # 1. Assume trading current expiry
        is_trade_next_expiry = False
        
        # 2. Find Current expiry date
        next_expiry_date = self.expirycalendar.get_next_expiry_date(
            date=datetime.datetime.today(), offset=1, series='weekly')
            
        # Find 2nd Trading from today
        day_2_from_today = self.nseholidays.next_trading_day(
            date=datetime.datetime.today(), offset=2)
        
        # if current expiry date is within next two trading days,
        # trade next expiry
        if day_2_from_today > next_expiry_date:
            is_next_expiry = True
            next_expiry_date = self.expirycalendar.get_next_expiry_date(
                date=datetime.datetime.today(), offset=2, series='weekly')

        expiry_date = next_expiry_date
        
        # We have underlying, strike, option_type and expiry.
        # all infomation to make the ticker_base.
       
        ticker_option_atm = Tickers.create_tickername_from_tickerparams(
                        ticker=underlying,
                        expiry=expiry_date,     
                        strike=strike, 
                        option_type=option_type,
                        expirycalendar=self.expirycalendar,
                        exchange='NFO',
                        datafeeder='ZERODHA')
            
            
            
        return ticker_option_atm
        


    def on_strategy_signal_event(self, strategy_signal_event):
        """
        Callback run when kanhoji.event.EventType_STRATEGYSIGNAL is 
        received.        

        Parameters
        ----------
        strategy_signal_event : kanhoji.event.EventType_StrategySignalEvent

        Returns
        -------
        None.
        
        Note
        ----
        `StrategySignalEvent` coming to this strategy has few deviations:
            a. ticker_base contains underlying/ticker_cash not ticker_base
            b. quantity contains no_of_lots as opposed to no_of_shares.
            c. price is just a reference price for finding ATM strike, rather than
            a price param for order.

        All of these are important field for processign the order and will be
        handled in this method.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        """
        try:
            sse = strategy_signal_event
            
            # in all manual*_ methods, pass price related params in 
            # PriceParser.display format (rg 113.25)

            # If an exit signal comes just exit all open positions
            if sse.strategy_signal_type == g.StrategySignalType_NEW_EXIT_SL:
                # Place exit_SL order
                # Do it for all open tickers.
                for ticker_base in self.tickers_invested:
                    actual_action = ('SELL' if 
                        self.tickers[ticker_base]['invested'] == -1  else 'BUY')
                                        
                    return self.manual_ticker_place_exit_order(
                        ticker_base=ticker_base, 
                        actual_action=actual_action,
                        stoploss_price=0, 
                        stoploss_trigger_price=0)   
                    
            elif sse.strategy_signal_type == g.StrategySignalType_EXIT_MARKET:
                # Place exit_SL order
                # Do it for all open tickers.                    
                for ticker_base in self.tickers_invested:
                    actual_action = ('SELL' if 
                        self.tickers[ticker_base]['invested'] == -1  else 'BUY')
                                        
                    return self.manual_ticker_place_exit_order(
                        ticker_base=ticker_base, 
                        actual_action=actual_action)                
                 
            elif sse.strategy_signal_type == g.StrategySignalType_NEW_ENTRY:
                if self.expirycalendar.is_expiry_day(series='weekly'):
                    return                
                assert sse.quantity > 0, "`quantity` in `NEW_ENTRY` `StrategySignalEvent` should be greater than 0, provided {}!"% (str(sse.quantity))
                
                # Entry allowed if no Entry already there
                is_entry = True if len(self.tickers_invested) == 0 else False
                ticker_option_atm = None
                logger.info("is_entry: {}".format(is_entry))
                logger.info("len(self.tickers_invested): {}".format(len(self.tickers_invested)))
                
                if is_entry:
                    # ticker_option_atm will get traded finally
                    ticker_option_atm = self.get_atm_ticker_from_strategy_signal_event(
                                            strategy_signal_event=sse)
                    
                    # #Note
                    # 1. Next Expiry tickers are present is self.tickers_base_extended
                    # 2. Next Expiry tickers are not present is self.tickers_base                    
                    # 3. Next Expiry tickers are present is self.tickers_proxy_objects,
                    #     self.tickers_proxy, self.tickers_proxy_base
                    # 4. Next Expiry tickers are present is self.price_handler.tickers
                    # 5. Next Expiry tickers are not present is self.price_handles.candles
                    # 6. As long as ticker_option_atm is present in self.tickers_base_extended,
                        # and candles are not required, it can be freely traded.
                logger.info("ticker_option_atm: {}".format(ticker_option_atm))
                
                if ticker_option_atm in self.tickers_base_extended:
                    # try and find if client info present.
                    # if present get client_ntuple
                    client = None
                    try:
                        if (
                                getattr(sse, 'broker', None) and 
                                getattr(sse, 'client_id', None) and 
                                getattr(sse, 'market', None) 
                            ):
                            client = self.get_client(
                                client_id=sse.client_id, 
                                market=sse.market, 
                                broker=sse.broker
                                )
                            logger.info("Got StrategySignalEvent for client : %s"%str(client))
                    except Exception as e:
                        logger.exception(e)
                        print (e)
                        client = None
                        
                    # sse.quantity = no of lots to be traded
                    no_of_shares = int(sse.quantity) * (
                        self.price_handler.tickers[ticker_option_atm]['lotsize']
                        )
                    
                    logger.info('Placing Entry order in ticker_base : {}, '
                        'actual_action: {}, quantity {}.'.format(
                            ticker_option_atm, 'BUY', no_of_shares)
                        )
                    
                    # the manual method below require price etc in 
                    # PriceParser.display (eg 125.25) format and not
                    # PriceParser.parse(1252500000) format.
                    if client:
                        # pass the client info as well to place order.
                        # Position is always 'BUY'
                        return self.manual_ticker_place_entry_order(
                            ticker_base=ticker_option_atm, 
                            actual_action='BUY',
                            no_of_shares=no_of_shares,
                            price=0,
                            trigger_price=0,
                            stoploss_price=0,
                            stoploss_trigger_price=0,
                            takeprofit_price=0,
                            client=client)
                    else:
                        # pass the client info as well to place order.
                        # Position is always 'BUY'
                        return self.manual_ticker_place_entry_order(
                            ticker_base=ticker_option_atm, 
                            actual_action='BUY',
                            no_of_shares=no_of_shares,
                            price=0,
                            trigger_price=0,
                            stoploss_price=0,
                            stoploss_trigger_price=0,
                            takeprofit_price=0
                            )
                        
        except Exception as e:
            logger.exception(e)
            
    def manual_ticker_place_entry_order(self, ticker_base, actual_action,
                    no_of_shares, LTP_base=None, price=None, trigger_price=None, 
                    stoploss_price=None, stoploss_trigger_price=None, 
                    takeprofit_price=None, LTP_futures=None, positions=None, 
                    is_set_tickers_proxy=False, **kwargs):
        """
        Manually place an New order or attach it to a callback. Used when price 
        related params are to be passed in  PriceParser.display (eg 125.25) 
        format and not PriceParser.parse(1252500000) format.

        It will convert these price params into PriceParser.parse form
        and call downstream methods.        

        Parameters
        ----------
        ticker_base : TYPE
            DESCRIPTION.
        actual_action : TYPE
            DESCRIPTION.
        no_of_shares : TYPE
            DESCRIPTION.
        LTP_base : TYPE, optional
            DESCRIPTION. The default is None.
        price : TYPE, optional
            DESCRIPTION. The default is None.
        trigger_price : TYPE, optional
            DESCRIPTION. The default is None.
        stoploss_price : TYPE, optional
            DESCRIPTION. The default is None.
        stoploss_trigger_price : TYPE, optional
            DESCRIPTION. The default is None.
        takeprofit_price : TYPE, optional
            DESCRIPTION. The default is None.
        LTP_futures : TYPE, optional
            DESCRIPTION. The default is None.
        positions : TYPE, optional
            DESCRIPTION. The default is None.
        is_set_tickers_proxy : TYPE, optional
            DESCRIPTION. The default is False.
        **kwargs : TYPE
            DESCRIPTION.

        Returns
        -------
        None.
        
        Example
        -------
        self.manual_ticker_place_entry_order(
            ticker_base='ACC', 
            actual_action='SELL',
            no_of_shares=12,
            price=1225.25,
            trigger_price=1224.25,
            stoploss_price=1287.25,
            stoploss_trigger_price=1286.25,
            takeprofit_price=1125.25)        

        """        
        
        # Usually it should be passed as we don't want extraneous signals coming
        # into Strategy by mistake.
        try:
            logger.info("Got a manual order for {0} in ticker_base"
            " {1}!!".format(actual_action, ticker_base)
            )
            
        except Exception as e:
            logger.exception(e)

        self._manual_ticker_place_entry_order(ticker_base=ticker_base, 
                actual_action=actual_action, no_of_shares=no_of_shares,
                LTP_base=LTP_base, price=price, 
                trigger_price=trigger_price, stoploss_price=stoploss_price, 
                stoploss_trigger_price=stoploss_trigger_price, 
                takeprofit_price=takeprofit_price, LTP_futures=LTP_futures, 
                positions=positions, is_set_tickers_proxy=is_set_tickers_proxy, 
                **kwargs)                    